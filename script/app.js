"use strict";

const btnIp = document.querySelector("#btn-ip");
const ul = document.createElement("ul");
let ipResponseUser;
btnIp.addEventListener("click", getIp);

async function getIp() {
  await fetch("https://api.ipify.org/?format=json")
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const { ip } = data;
      ipResponseUser = ip;
    });
  getHackerContacts();
}

async function getHackerContacts() {
  await getIp;
  await fetch(`http://ip-api.com/json/${ipResponseUser}`, {
    mode: "cors",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      const { timezone, country, regionName, city, as } = data;
      document.body.append(ul);
      ul.innerHTML += `<li>Континент взломщика : ${timezone}</li><li>Страна взломщика : ${country}</li><li>Регион взломщика :  ${regionName}</li><li>Город взломщика : ${city}</li><li>Район по координатам взлощика : ${as}</li>`;
      // континент, страна, регион, город, район города.
    });
}
